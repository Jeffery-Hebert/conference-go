from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page":1,
        "query": city + " " + state
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

def get_weather(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},us&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = json.loads(response.content)
    lat = content[0]['lat']
    lon = content[0]['lon']
    urll = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(urll)
    content = json.loads(response.content)
    try:
        return {"temp": content['main']["temp"],
                   "description":content['weather'][0]["description"]
                   }
    except (KeyError, IndexError):
        return None
