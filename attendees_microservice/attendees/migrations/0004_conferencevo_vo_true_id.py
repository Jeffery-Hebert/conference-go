# Generated by Django 4.0.3 on 2022-11-18 03:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0003_alter_accountvo_is_active_alter_accountvo_updated'),
    ]

    operations = [
        migrations.AddField(
            model_name='conferencevo',
            name='vo_true_id',
            field=models.IntegerField(null=True),
        ),
    ]
